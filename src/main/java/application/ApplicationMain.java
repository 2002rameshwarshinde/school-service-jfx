package application;




import addTeacher.AddTeacher;
import addexamSchedule.AddExamSchedule;
import deleteDiaryInfo.DeleteDiaryInfo;
import deleteExamSchedule.DeleteSchedule;
import deleteLeave.DeleteLeave;
import deleteNotice.DeleteNotice;
import deleteTimeTable.DeleteTimeTable;
import diaryInformation.DiaryInformation;
import javafx.application.Application;
import javafx.stage.Stage;
import searchDiaryInfo.SearchDiaryInfo;
import searchExamSchedule.SearchExamSchedule;
import searchFees.SearchFees;
import searchLeave.SearchLeave;
import searchLeaveStud.SearchLeaveStud;
import searchNotice.SearchNotice;
import searchTimeTable.SearchTimeTable;
import stageMaster.StageMaster;
import timeTable.TimeTable;
import updateDiaryInfo.UpdateDairyInfo;
import updateExamSchedule.UpdateExamSchedule;
import updateFees.UpdateFees;
import updateLeave.UpdateLeave;
import updateNotice.UpdateNotice;
import updateNotice.UpdateNoticeController;
import updateTimeTable.UpdateTimetable;

public class ApplicationMain extends Application {

	public static void main(String args[]) {

		launch(args);

	}

	@Override
	public void start(Stage primaryStage) {
		StageMaster.setStage(primaryStage);
	 new  AddTeacher().show();

	}
	
	
}
