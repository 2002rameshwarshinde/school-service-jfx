package updateFees;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import Common.ApiEndPoint;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

public class UpdateFeesController {

	@FXML
	private TextField grnNo;

	@FXML
	private TextField studentName;

	@FXML
	private ComboBox appliedclass;

	@FXML
	private TextField totalFees;

	@FXML
	private TextField feesPaid;

	@FXML
	private TextField RemainingFees;

	@FXML
	private DatePicker date;

	@FXML
	private Button AddUpdate;
	
	@FXML
	private TextField idnumber;
	
	 
	    private void updateData() {
	    	
	        String grnNoValue = grnNo.getText();
	        String studentNameValue = studentName.getText();
	        String appliedClassValue = appliedclass.getValue().toString(); 
	        String totalFeesValue = totalFees.getText();
	        String feesPaidValue = feesPaid.getText();
	        String remainingFeesValue = RemainingFees.getText();
	        String acadmicYearValue = date.getValue().toString();

	    	String IDnumber = idnumber.getText();

			String apiUrl = ApiEndPoint.UPDATEFEES + IDnumber;
	        
	        try {
	        	
	            URL url = new URL(apiUrl); 
	            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	            connection.setRequestMethod("PUT");
	            connection.setRequestProperty("Content-Type", "application/json");
	            connection.setDoOutput(true);
	           
	            String payload = "{\n" +
	                    "    \"grnNo\":\"" + grnNoValue + "\",\n" +
	                    "    \"studentName\":\"" + studentNameValue + "\",\n" +
	                    "    \"appliedClass\":\"" + appliedClassValue + "\",\n" +
	                    "    \"totalFees\":\"" + totalFeesValue + "\",\n" +
	                    "    \"feesPaid\":\"" + feesPaidValue + "\",\n" +
	                    "    \"remainingFees\":\"" + remainingFeesValue + "\",\n" +
	                    "    \"acadmicYear\":\"" + acadmicYearValue + "\"\n" +
	                    "}";

	            // Send the PUT request
	            try (DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream())) {
	                outputStream.writeBytes(payload);
	                outputStream.flush();
	            }
	            int responseCode = connection.getResponseCode();
	            if (responseCode == HttpURLConnection.HTTP_OK) {
	               
	                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	                String response = reader.readLine();
	                reader.close();
					 showAlert(Alert.AlertType.INFORMATION, "Update Successful", "The data has been successfully updated!");

	                System.out.println("Update Successful: " + response);
	            } else {
	            	 showAlert(Alert.AlertType.ERROR, "Update Failed", "Failed to update the data. Please try again later.");

	                System.out.println("Update Failed: " + responseCode);
	            }

	            connection.disconnect();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	    
	    public void addupdatedata(ActionEvent event) {
	    	updateData();
	    }
	    private void showAlert(Alert.AlertType type, String title, String content) {
	        Alert alert = new Alert(type);
	        alert.setTitle(title);
	        alert.setHeaderText(null);
	        alert.setContentText(content);
	        alert.showAndWait();
	    }
}
