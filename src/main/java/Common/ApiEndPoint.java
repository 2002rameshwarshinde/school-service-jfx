package Common;

public class ApiEndPoint {

	private final static String URL = "http://localhost:8080";
	public final static String ADMISSION = URL + "/admission/api/v1/add";
	public final static String REGISTRATION = URL + "/registration/api/v1/user";
	public final static String TIMETABLEENTRY = URL + "/Timetable/api/add";
	public final static String NOTICEENTRY = URL + "/Notice/api/create";
	public final static String FEESENTRY = URL + "/Fees/api/add";
	public final static String DELETEFEES = URL + "/Fees/api/delete/";
	public final static String LEAVEREQUEST = URL + "/Leave/api/request";
	public final static String SEARCHLEAVEBYSTATUS = URL + "/Leave/api/show/";
	public final static String SEARCHFEES = URL + "/fees/api/";
	public final static String SEARCHNOTICE = URL + "/Notice/api/Notices/";
	public final static String UPDATEFEES = URL + "/Fees/api/update/";
	public final static String DELETENOTICE = URL + "/Notice/api/noticeById/";
	public final static String UPDATENOTICE = URL + "/Notice/api/update/";
	public final static String UPDATELEAVE = URL + "/Leave/api/update/";
	public final static String DELETELEAVE = URL + "/Leave/api/Delete/";
	public final static String EXAMSCHEDULE = URL + "/Exam/api/Add";
	public final static String DELETESCHEDULE = URL + "/Exam/api/delete/";
	public final static String SEARCHEXAMSCHEDULE = URL + "/Exam/api/Find/";
	public final static String DIARYINFO = URL + "/Diary/Api/Add";
	public final static String DIARYINFOSEARCH = URL + "/Diary/Api/Find/";
	public final static String DELETEDIARY = URL + "/Diary/Api/Delete/";
	public final static String UPDATEDIARY = URL + "/Diary/Api/Update/";
	public final static String SEARCHTIMETABLE = URL + "/Timetable/api/";
	public final static String DELETETIMETABLE = URL + "/Timetable/Api/delete/";
	public final static String UPDATETIMETABLE = URL + "/Timetable/Api/update/";
    public final static String UPDATEEXAMSCHEDULE=URL+"/Exam/api/update/";
	public final static String SEARCHLEAVESTUD=URL+"/Leave/api/v1/request/";
	public final static String UPDATELEAVESTATUS=URL+"/Leave/api/v1/";
	public final static String ADDTEACHER=URL+"/Teacher/api/add";
}
