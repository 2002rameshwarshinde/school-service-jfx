package studentadmission;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class StudentController {
@FXML
private TextField studentName;

@FXML
private TextField fatherName;

@FXML
private TextField motherName;

@FXML
private TextField lastName;

@FXML
private TextField parentMob;

@FXML
private TextField address;


@FXML
private Button next;

public static TextField[] data = new TextField[6];




public void setStudentName(TextField studentName) {
	data[0] = studentName;
	this.studentName = studentName;
}

public TextField getFatherName() {
	return fatherName;
}

public void setFatherName(TextField fatherName) {
	data[1] = fatherName;

	this.fatherName = fatherName;
}

public TextField getMotherName() {
	return motherName;
}

public void setMotherName(TextField motherName) {
	data[2] = motherName;

	this.motherName = motherName;
}

public TextField getLastName() {
	return lastName;
}


public void setLastName(TextField lastName) {
	data[3] = motherName;
	this.lastName = lastName;
}

public TextField getparentMob() {

	return parentMob;
}

public void setParentMob(TextField parentMob) {
	data[4] = parentMob;

	this.parentMob = parentMob;
}

public TextField getAddress() {
	return address;
}

public void setAddress(TextField address) {
	data[5] = address;

	this.address = address;
}

public Button getNext() {
	return next;
}

public void setNext(Button next) {
	this.next = next;
}

StrValues str=new StrValues();
public void next(ActionEvent event) {
	str.setAddress(address.getText());
	str.setFatherName(fatherName.getText());
	str.setLastName(lastName.getText());
	str.setMotherName(motherName.getText());
	str.setParentMob(parentMob.getText());
	str.setStudentName(studentName.getText());


	new StudentGeneralInfoController().show();
}

}
