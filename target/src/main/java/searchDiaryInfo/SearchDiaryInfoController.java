package searchDiaryInfo;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.dnyanyog.entity.DiaryInfo;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import Common.ApiEndPoint;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import searchLeave.SearchLeaveController;

public class SearchDiaryInfoController {

	@FXML
	private TextField grnNo;
	
	
	@FXML
	private TableView<DiaryInfo>diaryInfoTable;
	
	@FXML
	private TableColumn<DiaryInfo,Long>idColumn;
	
	@FXML
	private TableColumn<DiaryInfo,Long>grnNOColumn;
	
	@FXML
	private TableColumn<DiaryInfo,String>studentNameColumn;
	
	@FXML
	private TableColumn<DiaryInfo,String>informationColumn;
	
	@FXML
	private TableColumn<DiaryInfo,LocalDate>dateColumn;
	
	@FXML
	private Button search;
	

	private static final Logger LOGGER = Logger.getLogger(SearchDiaryInfoController.class.getName());

	public void initialize() {
		idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
		grnNOColumn.setCellValueFactory(new PropertyValueFactory<>("grnNO"));
		studentNameColumn.setCellValueFactory(new PropertyValueFactory<>("studentName"));
		informationColumn.setCellValueFactory(new PropertyValueFactory<>("information"));
		dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
		
		
	}

	private void fetchDataAndPopulateTableView() {

		Object selectNumber = grnNo.getText();
		if (selectNumber == null) {
			LOGGER.log(Level.WARNING, "Please select a Status.");
			return;
		}

		String number = selectNumber.toString();

		String apiUrl = ApiEndPoint.DIARYINFOSEARCH + number;

		try {
			URL url = new URL(apiUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");

			int responseCode = connection.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_OK) {
				Scanner scanner = new Scanner(connection.getInputStream());
				StringBuilder response = new StringBuilder();
				while (scanner.hasNextLine()) {
					response.append(scanner.nextLine());
				}
				scanner.close();
				connection.disconnect();

				// Parse the JSON response and populate the TableView
				List<DiaryInfo> diaryInfo = parseJsonResponse(response.toString());
				diaryInfoTable.getItems().setAll(diaryInfo);
			} else {
				LOGGER.log(Level.WARNING, "Failed to fetch data from the backend API");
			}
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "An error occurred while fetching data from the backend API", e);
		}
	}

	private List<DiaryInfo> parseJsonResponse(String jsonResponse) {
	    List<DiaryInfo> diaryInfoList = new ArrayList<>();
	    try {
	        ObjectMapper objectMapper = new ObjectMapper();
	        JsonNode root = objectMapper.readTree(jsonResponse);

	        if (root.isArray()) {
	            for (JsonNode node : root) {
	                long id = node.get("id").asLong();
	                long grnNo = node.get("grnNO").asLong();
	                String studentName = node.get("studentName").asText();
	                String date = node.get("date").asText();
	                String information = node.get("information").asText();

	                DiaryInfo diaryInfo = new DiaryInfo();
	                diaryInfo.setId(id);
	                diaryInfo.setGrnNO(grnNo);
	                diaryInfo.setStudentName(studentName);
	                diaryInfo.setDate(LocalDate.parse(date));
	                diaryInfo.setInformation(information);
	                
	                diaryInfoList.add(diaryInfo); 

	            }
	        }
	    } catch (Exception e) {
	        LOGGER.log(Level.SEVERE, "An error occurred while parsing JSON response", e);
	    }

	    return diaryInfoList;
	}

	public void search(ActionEvent event) {
		fetchDataAndPopulateTableView();
	}
	
}
