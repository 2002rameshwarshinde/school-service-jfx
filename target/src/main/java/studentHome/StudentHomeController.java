package studentHome;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import login.Login;
import studentRegistration.Registration;

public class StudentHomeController {

	@FXML
	private Button registration;

  @FXML
  private Button Login;

  public void login(ActionEvent event) {
	  new Login().show();
  }

  public void registration(ActionEvent event) {
	  new Registration().show();
  }
}
