package studentadmission;

public class StrValues {

	private String studentName;
	private String fatherName;
	private String motherName;
	private String lastName;
	private String parentMob;
	private String  address;
	public static String[] data = new String[6];


	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		data[0] = studentName;

		this.studentName = studentName;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		data[1] = fatherName;

		this.fatherName = fatherName;
	}
	public String getMotherName() {
		data[2] = studentName;

		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getLastName() {
		data[3] = lastName;

		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getParentMob() {
		return parentMob;
	}
	public void setParentMob(String parentMob) {
		data[4] = parentMob;

		this.parentMob = parentMob;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		data[5] = address;

		this.address = address;
	}

}
