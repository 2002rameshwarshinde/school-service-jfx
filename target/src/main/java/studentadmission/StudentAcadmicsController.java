package studentadmission;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import Common.ApiEndPoint;
import Common.HttpUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import principal.Principal;

public class StudentAcadmicsController {

	@FXML
	private TextField birthDate;

	@FXML
	private ComboBox gender;

	@FXML
	private ComboBox classApply;

	@FXML
	private TextField schoolName;

	@FXML
	private TextField studentmobNo;

	@FXML
	private TextField email;

	@FXML
	private Button next;

	public void next(ActionEvent event) throws IOException {
		final String messageContent = "{\n" + "\"studentName\"" + ":\"" + StrValues.data[0] + "\", \r\n"
				+ "\"fatherName\"" + ":\"" + StrValues.data[1] + "\", \r\n" + "\"motherName\"" + ":\""
				+ StrValues.data[2]+ "\", \r\n" + "\"lastName\"" + ":\"" + StrValues.data[3] + "\", \r\n"
				+ "\"parentMobileNo\"" + ":\"" + StrValues.data[4]+ "\", \r\n" + "\"address\"" + ":\""
				+ StrValues.data[5] + "\", \r\n" + "\"birthDate\"" + ":\"" + birthDate.getText() + "\", \r\n"
				+ "\"gender\"" + ":\"" + gender.getValue() + "\", \r\n" + "\"appliedClass\"" + ":\""
				+ classApply.getValue() + "\", \r\n" + "\"previousSchoolName\"" + ":\"" + schoolName.getText()
				+ "\", \r\n" + "\"studentMob\"" + ":\"" + studentmobNo.getText() + "\", \r\n" + "\"email\"" + ":\""
				+ email.getText() + "\"\r\n" + "\n}";

		System.out.println(messageContent);
		HttpURLConnection connection = HttpUtil.PostJsonRequest(ApiEndPoint.ADMISSION, messageContent);
		int respCode = connection.getResponseCode();
		System.out.println("Response from the server is: \n");
		System.out.println("The POST Request Response Code :  " + respCode);
		System.out.println("The POST Request Response Message : " + connection.getResponseMessage());
		if (respCode == HttpURLConnection.HTTP_CREATED) {

			InputStreamReader irObj = new InputStreamReader(connection.getInputStream());
			BufferedReader br = new BufferedReader(irObj);
			String input = null;
			StringBuffer sb = new StringBuffer();
			while ((input = br.readLine()) != null) {
				sb.append(input);
			}
			br.close();
			connection.disconnect();

			System.out.println(sb.toString());

			new Principal().show();

		} else {

			System.out.println("POST Request did not work.");
		}
	}

}
